import string

def get_longest_word(s: str) -> str:
    """
    Get the longest word from a given string.

    Examples:
    >>> get_longest_word('This is a simple example.')
    'example'
    
    >>> get_longest_word('The quick brown fox jumps over the lazy dog')
    'quick'
    
    >>> get_longest_word('One')
    'One'
    
    >>> get_longest_word('')
    ''
    
    >>> get_longest_word('Python is fun!')
    'Python'
    
    >>> get_longest_word('The number is 123 and the word is abc')
    'number'
    
    >>> get_longest_word(123)
    Traceback (most recent call last):
        ...
    TypeError: input must be a string

    """
    if not isinstance(s, str):
        raise TypeError("input must be a string")

    # Remove punctuation and split the string into words
    translator = str.maketrans("", "", string.punctuation)
    words = s.translate(translator).split()

    # Filter out words with digits
    words = [word for word in words if any(char.isalpha() for char in word)]

    return max(words, key=len, default='')

if __name__ == "__main__":
    import doctest
    doctest.testmod()
