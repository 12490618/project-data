from fractions import Fraction

def get_fractions(a_b: str, c_b: str) -> str:
    """
    Splits the input strings containing fractions, adds them, and returns the result.

    :param a_b: A string representing the first fraction (e.g., '1/2')
    :param c_b: A string representing the second fraction (e.g., '3/4')
    :return: A string representing the sum of the fractions (e.g., '5/4')
    
    Examples:
    >>> get_fractions('1/2', '3/4')
    '5/4'

    >>> get_fractions('2/3', '1/6')
    '5/6'

    >>> get_fractions('1/4', '1/4')
    '1/2'

    >>> get_fractions('2/5', '1/5')
    '3/5'
    

    >>> get_fractions('1/2', 'invalid_fraction')
    'Error: Invalid fraction representation'
    >>> get_fractions('invalid_fraction', '1/2')
    'Error: Invalid fraction representation
    >>> get_fractions(a_b: str)
    'Error: Invalid fraction representation

    """
    if type(a_b, c_b) == str :
        pass
    else :
        print("error")
    try:
        fraction_a = Fraction(a_b)
        fraction_c = Fraction(c_b)
        result = fraction_a + fraction_c
        return str(result)
    except ValueError:
        return 'Error: Invalid fraction representation'


def test_get_fractions():
    """
    Test cases for get_fractions function.
    
    Examples:
    >>> test_get_fractions()
    """
    assert get_fractions('1/2', '3/4') == '5/4'
    assert get_fractions('2/3', '1/6') == '5/6'
    assert get_fractions('1/4', '1/4') == '1/2'
    assert get_fractions('2/5', '1/5') == '3/5'
    assert get_fractions('1/2', 'invalid_fraction') == 'Error: Invalid fraction representation'


print(get_fractions(2,3))
print(get_fractions(2,3))
print(get_fractions(2,3))
