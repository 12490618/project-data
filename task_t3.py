def replacer(s: str) -> str:
    """
    Replace vowels in a given string with '*'.

    Examples:
    >>> replacer('hello')
    'h*ll*'
    
    >>> replacer('Python is amazing')
    'Pyth*n *s *m*z*ng'

    If the string contains only vowels, replace with a single '*'
    >>> replacer('AEIOU')
    '*'
    
    If the string is empty, return an empty string
    >>> replacer('')
    ''
    
    >>> replacer(123)  # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    TypeError: ...
    
    #>>> replacer("*&!")
    #Traceback (most recent call last):
        #...
    #ValueError: Input must contain only letters.
    """
    if not isinstance(s, str):
        raise TypeError("Argument must be a string.")
    
    if s and s.isalpha() and set(s.lower()) <= set("aeiou"):
        return '*'
    else:
        return ''.join('*' if char in "AEIOUaeiou" else char for char in s)

if __name__ == "__main__":
    import doctest
    doctest.testmod()


